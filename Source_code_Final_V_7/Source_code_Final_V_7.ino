#include <Wire.h>
#include "RTClib.h"
#include <RF01.h>
#include<avr/io.h>
#include<TimerOne.h>
#include <stdint.h>
RTC_DS1307 RTC;
#define nactive HIGH
#define active LOW

//git

const int blinkPin = 4; 
const int latchPin = 5;  // Pin connected to Pin 12 of 74HC595 (Latch)
const int dataPin  = 6;  // Pin connected to Pin 14 of 74HC595 (Data)
const int clockPin = 7;  // Pin connected to Pin 11 of 74HC595 (Clock)
const int dutyPin = 8;
const int bellPin = 9;

unsigned long t1;
unsigned long t2;
String diterima = "";
uint32_t unixjam;
long pembanding = 0;
int kedip= 0;
int arrayalarm[20];
int tempalarm[20];
int arrayperiod[2];
int tempperiod[2];
int digit1;
int digit2;
int digit3;
int digit4;
String stringalarm = "";
char ceksumalarm;
int index = 0;
int up = 0;
int jumlahceksumalarm = 0;

DateTime sekarang;

// Describe each digit in terms of display segments
// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
const byte numbers[10] = {
                    0b11111100,
                    0b01100000,
                    0b11011010,
                    0b11110010,
                    0b01100110,
                    0b10110110,
                    0b10111110,
                    0b11100000,
                    0b11111110,
                    0b11110110,
};

void setup()
{
    Serial.begin(9600);
    Wire.begin();
    RTC.begin();
    
  rf01_prepAll();   
  //set pins to output 
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(bellPin, OUTPUT);
  pinMode(dutyPin, OUTPUT);
  pinMode(blinkPin,OUTPUT);
  digitalWrite(bellPin,LOW);
  digitalWrite(dutyPin,LOW);
  digitalWrite(blinkPin,HIGH);
  //RTC.adjust(DateTime(unixjam));
  //sekarang = RTC.now();
}

void loop()
{
    void(*count)();
    count = &update;
      /*if(kedip != sekarang.second())
      {
        digitalWrite(blinkPin, HIGH);
        delay(300);
        digitalWrite(blinkPin,LOW);
      }*/
      rf01_rxdata(rf01_data, 32, count);
      diterima += receiver();
      category(diterima);
      diterima = "";
      if(pembanding != unixjam)
      {
        RTC.adjust(DateTime(unixjam));
        pembanding = unixjam;
      }
  Serial.print(sekarang.hour(), DEC);
  Serial.print(':');
  Serial.print(sekarang.minute(), DEC);
  Serial.print(':');
  Serial.print(sekarang.second(), DEC);
  Serial.println();
  //Serial.println(millis());
  delay(1);  
}

char* receiver()
{
  //rf01_receive((*f)());
	char* buf = (char*) rf01_data;
        Serial.println(buf);
  return buf;
}

void update()
{
   sekarang = RTC.now();
   digit1 = sekarang.hour()/10;
   digit2  = sekarang.hour()%10;
   digit3 = sekarang.minute()/10;
   digit4  =  sekarang.minute()%10;
   show(numbers[digit1],numbers[digit2],numbers[digit3],numbers[digit4]);
   redup();
   if (dutyPin == HIGH)
   {
     alarm();
   } 
}

void show( byte puluhanjam,byte satuanjam,byte puluhanmenit,byte satuanmenit)
{
  // Use a loop and a bitwise AND to move over each bit that makes up
  // the seven segment display (from left to right, A => G), and check
  // to see if it should be on or not
  for(int j = 0; j <= 7; j++)
  {
    byte toWrite1 = puluhanjam & (0b10000000 >> j); 
    byte toWrite2 = satuanjam & (0b10000000 >> j);
    byte toWrite3 = puluhanmenit & (0b10000000 >> j); 
    byte toWrite4 = satuanmenit & (0b10000000 >> j); 

    // Otherwise shift it into the register
    shiftIt(toWrite4,toWrite3,toWrite2,toWrite1);
  }
}

void shiftIt (byte data1,byte data2,byte data3,byte data4)
{
    // Set latchPin LOW while clocking these 8 bits in to the register
    digitalWrite(latchPin, LOW);

    for (int k=0; k <= 7; k++)
    {
      // clockPin LOW prior to sending a bit
      digitalWrite(clockPin, LOW); 
      if ( data1 & (1 << k) )
      {
        digitalWrite(dataPin, active); // turn “On”
      }
      else
      {
        digitalWrite(dataPin, nactive); // turn “Off”
      }

      // and clock the bit in
      digitalWrite(clockPin, HIGH);
    }
    
    for (int l=0; l <= 7; l++)
    {
      // clockPin LOW prior to sending a bit
      digitalWrite(clockPin, LOW); 
      if ( data2 & (1 << l) )
      {
        digitalWrite(dataPin, active); // turn “On”
      }
      else
      {
        digitalWrite(dataPin, nactive); // turn “Off”
      }

      // and clock the bit in
      digitalWrite(clockPin, HIGH);
    }
    
    for (int m=0; m <= 7; m++)
    {
      // clockPin LOW prior to sending a bit
      digitalWrite(clockPin, LOW); 
      if ( data3 & (1 << m) )
      {
        digitalWrite(dataPin, active); // turn “On”
      }
      else
      {
        digitalWrite(dataPin, nactive); // turn “Off”
      }

      // and clock the bit in
      digitalWrite(clockPin, HIGH);
    }
    
    for (int n=0; n <= 7; n++)
    {
      // clockPin LOW prior to sending a bit
      digitalWrite(clockPin, LOW); 
      if ( data4 & (1 << n) )
     {
        digitalWrite(dataPin, active); // turn “On”
      }
      else
      {
        digitalWrite(dataPin, nactive); // turn “Off”
      }
      // and clock the bit in
      digitalWrite(clockPin, HIGH);
    }

    //stop shifting out data
    digitalWrite(clockPin, LOW); 

    //set latchPin to high to lock and send data
    digitalWrite(latchPin, HIGH);
   
}

void alarm ()
{
  int jamalarm;
  int menitalarm;
    for(int al=0; al<20; al++)
    {
      jamalarm = arrayalarm[al]/60;
      menitalarm = arrayalarm[al]%60;
      if (digit4 == menitalarm%10 && sekarang.second()>1 &&sekarang.second()<3)
      {
        if (digit3 == menitalarm/10)
        {
          if (digit2 == jamalarm%10)
          {
            if (digit1 == jamalarm/10)
            {
              digitalWrite(bellPin, HIGH);
              delay(300);
              digitalWrite(bellPin,LOW);
            }
          }
        }
      }
    }
}




void category(String tipedata)
{
  
  if (tipedata.charAt(0)=='A')
  {
    String stringjam = "";
    char ceksumjam;
    int jumlahceksum=0;
    int tp=0;
    for(int t=1; t < tipedata.indexOf('S'); t++)
    {
     stringjam += tipedata.charAt(t);
     tp++;
     jumlahceksum += tipedata.charAt(t);
    }
    ceksumjam += tipedata.charAt(tp + 2);
        
    if(((jumlahceksum % 94) + 33) == ceksumjam)
    {
      unixjam = stringjam.toInt();
    }
  }
  else if(tipedata.charAt(0)=='B')
  {
    for(int u=1; u < 32; u++)
    {
      if (tipedata.charAt(u)=='A')
      {
        tempalarm[index] = stringalarm.toInt();
        stringalarm = "";
        index++;
        up++;
      }
      else if (tipedata.charAt(u)=='S')
      {
        up++;
        ceksumalarm += tipedata.charAt(up + 1);
        stringalarm = "";
        index = 0;
        break;
       }
      else
      {
        stringalarm += tipedata.charAt(u);
        up++;
        jumlahceksumalarm += tipedata.charAt(u);
      }
    }
    up = 0;
    if(((jumlahceksumalarm % 94) + 33) == ceksumalarm)
    {
      for (int p=0;p<20;p++)
      {
        arrayalarm[p]=tempalarm[p];
      }
      memset(tempalarm,0,20);
    }    
  }
  else if(tipedata.charAt(0)=='C')
  {
    String stringperiod = "";
    char ceksumperiod;
    int jumlahceksumperiod = 0;
    int vp=0;
    for(int v=1; v < tipedata.indexOf('S'); v++)
    {
      if (tipedata.charAt(v)=='A')
      {
        tempperiod[0] = stringperiod.toInt();
        stringperiod = "";
        vp++;
      }
      else
      {
        stringperiod += tipedata.charAt(v);
        vp++;
        jumlahceksumperiod += tipedata.charAt(v);
      }
    }
    tempperiod[1] = stringperiod.toInt();
    ceksumperiod += tipedata.charAt(vp + 2);
    stringperiod = "";
    if(((jumlahceksumperiod % 94) + 33) == ceksumperiod)
    {
      for (int o=0;o<2;o++)
      {
        arrayperiod[o]=tempperiod[o];
      }
      memset(tempperiod,0,2);
    }
  }
}

void redup()
{
      if ((((sekarang.hour()+7)*60)+sekarang.minute())>= arrayperiod[1])
      {
              digitalWrite(dutyPin, LOW);
      }
      else if ((((sekarang.hour()+7)*60)+sekarang.minute())>= arrayperiod[0])
      {
              digitalWrite(dutyPin, HIGH);
      }
}
